const accordionItems = document.querySelectorAll('.accordion-item')

function toggleItem(item) {
  const isOpened = item.dataset.opened === 'true'
  renderItem(item, !isOpened)
}

function renderItem(item, isOpened) {
  const button = item.querySelector('button')
  const panel = item.querySelector('.accordion-panel')
  const iconPlus = item.querySelector('.header-icon-plus')
  const iconMinus = item.querySelector('.header-icon-minus')

  item.dataset.opened = isOpened
  button.ariaExpanded = isOpened

  if (isOpened) {
    panel.classList.add('panel-opened')
    iconPlus.classList.add('hidden')
    iconMinus.classList.remove('hidden')
  } else {
    panel.classList.remove('panel-opened')
    iconPlus.classList.remove('hidden')
    iconMinus.classList.add('hidden')
  }
}

accordionItems.forEach(item => item.querySelector('button').addEventListener('click', () => toggleItem(item)))
